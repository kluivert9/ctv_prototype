import React from 'react'
import Link from 'next/link'

const Error = ({ statusCode, title }) => {
  return (
    <div style={{textAlign: 'center', color: 'tomato'}}>

      {statusCode
        ? `Ошибка сервера ${statusCode}`
        : <h2>Ошибка клиента 404 {title}</h2>}

      <br />

      <Link href='/'>
        <a>на главную</a>
      </Link>

    </div>
  )
}

Error.getInitialProps = ({ res, err }) => {
  const statusCode = res ? res.statusCode : err ? err.statusCode : 404
  return {statusCode}
}

export default Error
