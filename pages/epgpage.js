import React from 'react'
import MyLayout from '../components/MyLayout';
import MetaHead from '../components/MetaHead';
import fetch from "isomorphic-unfetch";
import Channel from "./channel/[url_protocol]";

const Epgpage = data => {
  //console.log(data)
  return (
    <MyLayout>
      <MetaHead
        name={'Телепрограмма'}
        keywords={'Ключевые слова страницы телепрограмм'}
        description={'Описание страницы телепрограмм'}
        canonical={'http://localhost:3000/epgpage'}/>
      <div className="container-fluid">
        <div className="row">
          <nav className="navbar navbar-expand tv-channel-filter pt-0 mt-0 mb-2">
            <ul className="navbar-nav mr-auto">
              <li className="nav-item"><a className="nav-link" href="#">Пн-10</a></li>
              <li className="nav-item"><a className="nav-link" href="#">Вт-11</a></li>
              <li className="nav-item active"><a className="nav-link" href="#">Сегодня</a></li>
              <li className="nav-item"><a className="nav-link" href="#">Чт-13</a></li>
              <li className="nav-item"><a className="nav-link" href="#">Пт-14</a></li>
              <li className="nav-item"><a className="nav-link" href="#">Сб-15</a></li>
              <li className="nav-item"><a className="nav-link" href="#">Вс-16</a></li>
            </ul>
          </nav>
        </div>
        <div className="tv-channel-list row">
          <div className="col-xs-1 col-sm-4 col-md-4 col-xl-2">
            <div className="card border-0 rounded-0 mb-4 tv-channel">
              <div className="tv-channel-title">
                <a className="d-flex align-items-center" href="#"><img className="mr-2" src="images/rossia1_logo.png"
                                                                       alt=""/>Россия 1</a>
              </div>
              <div className="p-1 d-flex align-items-start">
                <div><span className="tv-time align-top mr-2 text-14 text-muted">06:30</span></div>
                <div className="tv-program"><span className="align-top"><a href="#">Русское лото</a></span></div>
              </div>
              <div className="p-1 d-flex align-items-start">
                <div><span className="tv-time align-top mr-2 text-14 text-muted">07:30</span></div>
                <div className="tv-program"><span className="align-top"><a href="#">Пусть говорят</a></span></div>
              </div>
              <div className="p-1 d-flex align-items-start active">
                <div><span className="tv-time align-top mr-2 text-14 text-muted">09:00</span></div>
                <div className="tv-program"><span className="align-top"><a href="#">Каникулы</a></span></div>
              </div>
              <div className="p-1 d-flex align-items-start">
                <div><span className="tv-time align-top mr-2 text-14 text-muted">12:00</span></div>
                <div className="tv-program"><span className="align-top"><a href="#">Х/ф Холоп</a></span></div>
              </div>
              <div className="p-1 d-flex align-items-start">
                <div><span className="tv-time align-top mr-2 text-14 text-muted">13:00</span></div>
                <div className="tv-program"><span className="align-top"><a href="#">Судьба человека с&nbsp;Борисом Корчевниковым</a></span>
                </div>
              </div>
            </div>
          </div>
          <div className="col-xs-1 col-sm-4 col-md-4 col-xl-2">
            <div className="card border-0 rounded-0 mb-4 tv-channel">
              <div className="tv-channel-title">
                <a className="d-flex align-items-center" href="#"><img className="mr-2" src="images/rossia1_logo.png"
                                                                       alt=""/>Россия 1</a>
              </div>
              <div className="p-1 d-flex align-items-start">
                <div><span className="tv-time align-top mr-2 text-14 text-muted">06:30</span></div>
                <div className="tv-program"><span className="align-top"><a href="#">Русское лото</a></span></div>
              </div>
              <div className="p-1 d-flex align-items-start">
                <div><span className="tv-time align-top mr-2 text-14 text-muted">07:30</span></div>
                <div className="tv-program"><span className="align-top"><a href="#">Пусть говорят</a></span></div>
              </div>
              <div className="p-1 d-flex align-items-start active">
                <div><span className="tv-time align-top mr-2 text-14 text-muted">09:00</span></div>
                <div className="tv-program"><span className="align-top"><a href="#">Каникулы</a></span></div>
              </div>
              <div className="p-1 d-flex align-items-start">
                <div><span className="tv-time align-top mr-2 text-14 text-muted">12:00</span></div>
                <div className="tv-program"><span className="align-top"><a href="#">Х/ф Холоп</a></span></div>
              </div>
              <div className="p-1 d-flex align-items-start">
                <div><span className="tv-time align-top mr-2 text-14 text-muted">13:00</span></div>
                <div className="tv-program"><span className="align-top"><a href="#">Судьба человека с&nbsp;Борисом Корчевниковым</a></span>
                </div>
              </div>
            </div>
          </div>
          <div className="col-xs-1 col-sm-4 col-md-4 col-xl-2">
            <div className="card border-0 rounded-0 mb-4 tv-channel">
              <div className="tv-channel-title">
                <a className="d-flex align-items-center" href="#"><img className="mr-2" src="images/rossia1_logo.png"
                                                                       alt=""/>Россия 1</a>
              </div>
              <div className="p-1 d-flex align-items-start">
                <div><span className="tv-time align-top mr-2 text-14 text-muted">06:30</span></div>
                <div className="tv-program"><span className="align-top"><a href="#">Русское лото</a></span></div>
              </div>
              <div className="p-1 d-flex align-items-start">
                <div><span className="tv-time align-top mr-2 text-14 text-muted">07:30</span></div>
                <div className="tv-program"><span className="align-top"><a href="#">Пусть говорят</a></span></div>
              </div>
              <div className="p-1 d-flex align-items-start active">
                <div><span className="tv-time align-top mr-2 text-14 text-muted">09:00</span></div>
                <div className="tv-program"><span className="align-top"><a href="#">Каникулы</a></span></div>
              </div>
              <div className="p-1 d-flex align-items-start">
                <div><span className="tv-time align-top mr-2 text-14 text-muted">12:00</span></div>
                <div className="tv-program"><span className="align-top"><a href="#">Х/ф Холоп</a></span></div>
              </div>
              <div className="p-1 d-flex align-items-start">
                <div><span className="tv-time align-top mr-2 text-14 text-muted">13:00</span></div>
                <div className="tv-program"><span className="align-top"><a href="#">Судьба человека с&nbsp;Борисом Корчевниковым</a></span>
                </div>
              </div>
            </div>
          </div>
          <div className="col-xs-1 col-sm-4 col-md-4 col-xl-2">
            <div className="card border-0 rounded-0 mb-4 tv-channel">
              <div className="tv-channel-title">
                <a className="d-flex align-items-center" href="#"><img className="mr-2" src="images/rossia1_logo.png"
                                                                       alt=""/>Россия 1</a>
              </div>
              <div className="p-1 d-flex align-items-start">
                <div><span className="tv-time align-top mr-2 text-14 text-muted">06:30</span></div>
                <div className="tv-program"><span className="align-top"><a href="#">Русское лото</a></span></div>
              </div>
              <div className="p-1 d-flex align-items-start">
                <div><span className="tv-time align-top mr-2 text-14 text-muted">07:30</span></div>
                <div className="tv-program"><span className="align-top"><a href="#">Пусть говорят</a></span></div>
              </div>
              <div className="p-1 d-flex align-items-start active">
                <div><span className="tv-time align-top mr-2 text-14 text-muted">09:00</span></div>
                <div className="tv-program"><span className="align-top"><a href="#">Каникулы</a></span></div>
              </div>
              <div className="p-1 d-flex align-items-start">
                <div><span className="tv-time align-top mr-2 text-14 text-muted">12:00</span></div>
                <div className="tv-program"><span className="align-top"><a href="#">Х/ф Холоп</a></span></div>
              </div>
              <div className="p-1 d-flex align-items-start">
                <div><span className="tv-time align-top mr-2 text-14 text-muted">13:00</span></div>
                <div className="tv-program"><span className="align-top"><a href="#">Судьба человека с&nbsp;Борисом Корчевниковым</a></span>
                </div>
              </div>
            </div>
          </div>
          <div className="col-xs-1 col-sm-4 col-md-4 col-xl-2">
            <div className="card border-0 rounded-0 mb-4 tv-channel">
              <div className="tv-channel-title">
                <a className="d-flex align-items-center" href="#"><img className="mr-2" src="images/rossia1_logo.png"
                                                                       alt=""/>Россия 1</a>
              </div>
              <div className="p-1 d-flex align-items-start">
                <div><span className="tv-time align-top mr-2 text-14 text-muted">06:30</span></div>
                <div className="tv-program"><span className="align-top"><a href="#">Русское лото</a></span></div>
              </div>
              <div className="p-1 d-flex align-items-start">
                <div><span className="tv-time align-top mr-2 text-14 text-muted">07:30</span></div>
                <div className="tv-program"><span className="align-top"><a href="#">Пусть говорят</a></span></div>
              </div>
              <div className="p-1 d-flex align-items-start active">
                <div><span className="tv-time align-top mr-2 text-14 text-muted">09:00</span></div>
                <div className="tv-program"><span className="align-top"><a href="#">Каникулы</a></span></div>
              </div>
              <div className="p-1 d-flex align-items-start">
                <div><span className="tv-time align-top mr-2 text-14 text-muted">12:00</span></div>
                <div className="tv-program"><span className="align-top"><a href="#">Х/ф Холоп</a></span></div>
              </div>
              <div className="p-1 d-flex align-items-start">
                <div><span className="tv-time align-top mr-2 text-14 text-muted">13:00</span></div>
                <div className="tv-program"><span className="align-top"><a href="#">Судьба человека с&nbsp;Борисом Корчевниковым</a></span>
                </div>
              </div>
            </div>
          </div>
          <div className="col-xs-1 col-sm-4 col-md-4 col-xl-2">
            <div className="card border-0 rounded-0 mb-4 tv-channel">
              <div className="tv-channel-title">
                <a className="d-flex align-items-center" href="#"><img className="mr-2" src="images/rossia1_logo.png"
                                                                       alt=""/>Россия 1</a>
              </div>
              <div className="p-1 d-flex align-items-start">
                <div><span className="tv-time align-top mr-2 text-14 text-muted">06:30</span></div>
                <div className="tv-program"><span className="align-top"><a href="#">Русское лото</a></span></div>
              </div>
              <div className="p-1 d-flex align-items-start">
                <div><span className="tv-time align-top mr-2 text-14 text-muted">07:30</span></div>
                <div className="tv-program"><span className="align-top"><a href="#">Пусть говорят</a></span></div>
              </div>
              <div className="p-1 d-flex align-items-start active">
                <div><span className="tv-time align-top mr-2 text-14 text-muted">09:00</span></div>
                <div className="tv-program"><span className="align-top"><a href="#">Каникулы</a></span></div>
              </div>
              <div className="p-1 d-flex align-items-start">
                <div><span className="tv-time align-top mr-2 text-14 text-muted">12:00</span></div>
                <div className="tv-program"><span className="align-top"><a href="#">Х/ф Холоп</a></span></div>
              </div>
              <div className="p-1 d-flex align-items-start">
                <div><span className="tv-time align-top mr-2 text-14 text-muted">13:00</span></div>
                <div className="tv-program"><span className="align-top"><a href="#">Судьба человека с&nbsp;Борисом Корчевниковым</a></span>
                </div>
              </div>
            </div>
          </div>
          <div className="col-xs-1 col-sm-4 col-md-4 col-xl-2">
            <div className="card border-0 rounded-0 mb-4 tv-channel">
              <div className="tv-channel-title">
                <a className="d-flex align-items-center" href="#"><img className="mr-2" src="images/rossia1_logo.png"
                                                                       alt=""/>Россия 1</a>
              </div>
              <div className="p-1 d-flex align-items-start">
                <div><span className="tv-time align-top mr-2 text-14 text-muted">06:30</span></div>
                <div className="tv-program"><span className="align-top"><a href="#">Русское лото</a></span></div>
              </div>
              <div className="p-1 d-flex align-items-start">
                <div><span className="tv-time align-top mr-2 text-14 text-muted">07:30</span></div>
                <div className="tv-program"><span className="align-top"><a href="#">Пусть говорят</a></span></div>
              </div>
              <div className="p-1 d-flex align-items-start active">
                <div><span className="tv-time align-top mr-2 text-14 text-muted">09:00</span></div>
                <div className="tv-program"><span className="align-top"><a href="#">Каникулы</a></span></div>
              </div>
              <div className="p-1 d-flex align-items-start">
                <div><span className="tv-time align-top mr-2 text-14 text-muted">12:00</span></div>
                <div className="tv-program"><span className="align-top"><a href="#">Х/ф Холоп</a></span></div>
              </div>
              <div className="p-1 d-flex align-items-start">
                <div><span className="tv-time align-top mr-2 text-14 text-muted">13:00</span></div>
                <div className="tv-program"><span className="align-top"><a href="#">Судьба человека с&nbsp;Борисом Корчевниковым</a></span>
                </div>
              </div>
            </div>
          </div>
          <div className="col-xs-1 col-sm-4 col-md-4 col-xl-2">
            <div className="card border-0 rounded-0 mb-4 tv-channel">
              <div className="tv-channel-title">
                <a className="d-flex align-items-center" href="#"><img className="mr-2" src="images/rossia1_logo.png"
                                                                       alt=""/>Россия 1</a>
              </div>
              <div className="p-1 d-flex align-items-start">
                <div><span className="tv-time align-top mr-2 text-14 text-muted">06:30</span></div>
                <div className="tv-program"><span className="align-top"><a href="#">Русское лото</a></span></div>
              </div>
              <div className="p-1 d-flex align-items-start">
                <div><span className="tv-time align-top mr-2 text-14 text-muted">07:30</span></div>
                <div className="tv-program"><span className="align-top"><a href="#">Пусть говорят</a></span></div>
              </div>
              <div className="p-1 d-flex align-items-start active">
                <div><span className="tv-time align-top mr-2 text-14 text-muted">09:00</span></div>
                <div className="tv-program"><span className="align-top"><a href="#">Каникулы</a></span></div>
              </div>
              <div className="p-1 d-flex align-items-start">
                <div><span className="tv-time align-top mr-2 text-14 text-muted">12:00</span></div>
                <div className="tv-program"><span className="align-top"><a href="#">Х/ф Холоп</a></span></div>
              </div>
              <div className="p-1 d-flex align-items-start">
                <div><span className="tv-time align-top mr-2 text-14 text-muted">13:00</span></div>
                <div className="tv-program"><span className="align-top"><a href="#">Судьба человека с&nbsp;Борисом Корчевниковым</a></span>
                </div>
              </div>
            </div>
          </div>
          <div className="col-xs-1 col-sm-4 col-md-4 col-xl-2">
            <div className="card border-0 rounded-0 mb-4 tv-channel">
              <div className="tv-channel-title">
                <a className="d-flex align-items-center" href="#"><img className="mr-2" src="images/rossia1_logo.png"
                                                                       alt=""/>Россия 1</a>
              </div>
              <div className="p-1 d-flex align-items-start">
                <div><span className="tv-time align-top mr-2 text-14 text-muted">06:30</span></div>
                <div className="tv-program"><span className="align-top"><a href="#">Русское лото</a></span></div>
              </div>
              <div className="p-1 d-flex align-items-start">
                <div><span className="tv-time align-top mr-2 text-14 text-muted">07:30</span></div>
                <div className="tv-program"><span className="align-top"><a href="#">Пусть говорят</a></span></div>
              </div>
              <div className="p-1 d-flex align-items-start active">
                <div><span className="tv-time align-top mr-2 text-14 text-muted">09:00</span></div>
                <div className="tv-program"><span className="align-top"><a href="#">Каникулы</a></span></div>
              </div>
              <div className="p-1 d-flex align-items-start">
                <div><span className="tv-time align-top mr-2 text-14 text-muted">12:00</span></div>
                <div className="tv-program"><span className="align-top"><a href="#">Х/ф Холоп</a></span></div>
              </div>
              <div className="p-1 d-flex align-items-start">
                <div><span className="tv-time align-top mr-2 text-14 text-muted">13:00</span></div>
                <div className="tv-program"><span className="align-top"><a href="#">Судьба человека с&nbsp;Борисом Корчевниковым</a></span>
                </div>
              </div>
            </div>
          </div>
          <div className="col-xs-1 col-sm-4 col-md-4 col-xl-2">
            <div className="card border-0 rounded-0 mb-4 tv-channel">
              <div className="tv-channel-title">
                <a className="d-flex align-items-center" href="#"><img className="mr-2" src="images/rossia1_logo.png"
                                                                       alt=""/>Россия 1</a>
              </div>
              <div className="p-1 d-flex align-items-start">
                <div><span className="tv-time align-top mr-2 text-14 text-muted">06:30</span></div>
                <div className="tv-program"><span className="align-top"><a href="#">Русское лото</a></span></div>
              </div>
              <div className="p-1 d-flex align-items-start">
                <div><span className="tv-time align-top mr-2 text-14 text-muted">07:30</span></div>
                <div className="tv-program"><span className="align-top"><a href="#">Пусть говорят</a></span></div>
              </div>
              <div className="p-1 d-flex align-items-start active">
                <div><span className="tv-time align-top mr-2 text-14 text-muted">09:00</span></div>
                <div className="tv-program"><span className="align-top"><a href="#">Каникулы</a></span></div>
              </div>
              <div className="p-1 d-flex align-items-start">
                <div><span className="tv-time align-top mr-2 text-14 text-muted">12:00</span></div>
                <div className="tv-program"><span className="align-top"><a href="#">Х/ф Холоп</a></span></div>
              </div>
              <div className="p-1 d-flex align-items-start">
                <div><span className="tv-time align-top mr-2 text-14 text-muted">13:00</span></div>
                <div className="tv-program"><span className="align-top"><a href="#">Судьба человека с&nbsp;Борисом Корчевниковым</a></span>
                </div>
              </div>
            </div>
          </div>
          <div className="col-xs-1 col-sm-4 col-md-4 col-xl-2">
            <div className="card border-0 rounded-0 mb-4 tv-channel">
              <div className="tv-channel-title">
                <a className="d-flex align-items-center" href="#"><img className="mr-2" src="images/rossia1_logo.png"
                                                                       alt=""/>Россия 1</a>
              </div>
              <div className="p-1 d-flex align-items-start">
                <div><span className="tv-time align-top mr-2 text-14 text-muted">06:30</span></div>
                <div className="tv-program"><span className="align-top"><a href="#">Русское лото</a></span></div>
              </div>
              <div className="p-1 d-flex align-items-start">
                <div><span className="tv-time align-top mr-2 text-14 text-muted">07:30</span></div>
                <div className="tv-program"><span className="align-top"><a href="#">Пусть говорят</a></span></div>
              </div>
              <div className="p-1 d-flex align-items-start active">
                <div><span className="tv-time align-top mr-2 text-14 text-muted">09:00</span></div>
                <div className="tv-program"><span className="align-top"><a href="#">Каникулы</a></span></div>
              </div>
              <div className="p-1 d-flex align-items-start">
                <div><span className="tv-time align-top mr-2 text-14 text-muted">12:00</span></div>
                <div className="tv-program"><span className="align-top"><a href="#">Х/ф Холоп</a></span></div>
              </div>
              <div className="p-1 d-flex align-items-start">
                <div><span className="tv-time align-top mr-2 text-14 text-muted">13:00</span></div>
                <div className="tv-program"><span className="align-top"><a href="#">Судьба человека с&nbsp;Борисом Корчевниковым</a></span>
                </div>
              </div>
            </div>
          </div>
          <div className="col-xs-1 col-sm-4 col-md-4 col-xl-2">
            <div className="card border-0 rounded-0 mb-4 tv-channel">
              <div className="tv-channel-title">
                <a className="d-flex align-items-center" href="#"><img className="mr-2" src="images/rossia1_logo.png"
                                                                       alt=""/>Россия 1</a>
              </div>
              <div className="p-1 d-flex align-items-start">
                <div><span className="tv-time align-top mr-2 text-14 text-muted">06:30</span></div>
                <div className="tv-program"><span className="align-top"><a href="#">Русское лото</a></span></div>
              </div>
              <div className="p-1 d-flex align-items-start">
                <div><span className="tv-time align-top mr-2 text-14 text-muted">07:30</span></div>
                <div className="tv-program"><span className="align-top"><a href="#">Пусть говорят</a></span></div>
              </div>
              <div className="p-1 d-flex align-items-start active">
                <div><span className="tv-time align-top mr-2 text-14 text-muted">09:00</span></div>
                <div className="tv-program"><span className="align-top"><a href="#">Каникулы</a></span></div>
              </div>
              <div className="p-1 d-flex align-items-start">
                <div><span className="tv-time align-top mr-2 text-14 text-muted">12:00</span></div>
                <div className="tv-program"><span className="align-top"><a href="#">Х/ф Холоп</a></span></div>
              </div>
              <div className="p-1 d-flex align-items-start">
                <div><span className="tv-time align-top mr-2 text-14 text-muted">13:00</span></div>
                <div className="tv-program"><span className="align-top"><a href="#">Судьба человека с&nbsp;Борисом Корчевниковым</a></span>
                </div>
              </div>
            </div>
          </div>
          <div className="col-xs-1 col-sm-4 col-md-4 col-xl-2">
            <div className="card border-0 rounded-0 mb-4 tv-channel">
              <div className="tv-channel-title">
                <a className="d-flex align-items-center" href="#"><img className="mr-2" src="images/rossia1_logo.png"
                                                                       alt=""/>Россия 1</a>
              </div>
              <div className="p-1 d-flex align-items-start">
                <div><span className="tv-time align-top mr-2 text-14 text-muted">06:30</span></div>
                <div className="tv-program"><span className="align-top"><a href="#">Русское лото</a></span></div>
              </div>
              <div className="p-1 d-flex align-items-start">
                <div><span className="tv-time align-top mr-2 text-14 text-muted">07:30</span></div>
                <div className="tv-program"><span className="align-top"><a href="#">Пусть говорят</a></span></div>
              </div>
              <div className="p-1 d-flex align-items-start active">
                <div><span className="tv-time align-top mr-2 text-14 text-muted">09:00</span></div>
                <div className="tv-program"><span className="align-top"><a href="#">Каникулы</a></span></div>
              </div>
              <div className="p-1 d-flex align-items-start">
                <div><span className="tv-time align-top mr-2 text-14 text-muted">12:00</span></div>
                <div className="tv-program"><span className="align-top"><a href="#">Х/ф Холоп</a></span></div>
              </div>
              <div className="p-1 d-flex align-items-start">
                <div><span className="tv-time align-top mr-2 text-14 text-muted">13:00</span></div>
                <div className="tv-program"><span className="align-top"><a href="#">Судьба человека с&nbsp;Борисом Корчевниковым</a></span>
                </div>
              </div>
            </div>
          </div>
          <div className="col-xs-1 col-sm-4 col-md-4 col-xl-2">
            <div className="card border-0 rounded-0 mb-4 tv-channel">
              <div className="tv-channel-title">
                <a className="d-flex align-items-center" href="#"><img className="mr-2" src="images/rossia1_logo.png"
                                                                       alt=""/>Россия 1</a>
              </div>
              <div className="p-1 d-flex align-items-start">
                <div><span className="tv-time align-top mr-2 text-14 text-muted">06:30</span></div>
                <div className="tv-program"><span className="align-top"><a href="#">Русское лото</a></span></div>
              </div>
              <div className="p-1 d-flex align-items-start">
                <div><span className="tv-time align-top mr-2 text-14 text-muted">07:30</span></div>
                <div className="tv-program"><span className="align-top"><a href="#">Пусть говорят</a></span></div>
              </div>
              <div className="p-1 d-flex align-items-start active">
                <div><span className="tv-time align-top mr-2 text-14 text-muted">09:00</span></div>
                <div className="tv-program"><span className="align-top"><a href="#">Каникулы</a></span></div>
              </div>
              <div className="p-1 d-flex align-items-start">
                <div><span className="tv-time align-top mr-2 text-14 text-muted">12:00</span></div>
                <div className="tv-program"><span className="align-top"><a href="#">Х/ф Холоп</a></span></div>
              </div>
              <div className="p-1 d-flex align-items-start">
                <div><span className="tv-time align-top mr-2 text-14 text-muted">13:00</span></div>
                <div className="tv-program"><span className="align-top"><a href="#">Судьба человека с&nbsp;Борисом Корчевниковым</a></span>
                </div>
              </div>
            </div>
          </div>
          <div className="col-xs-1 col-sm-4 col-md-4 col-xl-2">
            <div className="card border-0 rounded-0 mb-4 tv-channel">
              <div className="tv-channel-title">
                <a className="d-flex align-items-center" href="#"><img className="mr-2" src="images/rossia1_logo.png"
                                                                       alt=""/>Россия 1</a>
              </div>
              <div className="p-1 d-flex align-items-start">
                <div><span className="tv-time align-top mr-2 text-14 text-muted">06:30</span></div>
                <div className="tv-program"><span className="align-top"><a href="#">Русское лото</a></span></div>
              </div>
              <div className="p-1 d-flex align-items-start">
                <div><span className="tv-time align-top mr-2 text-14 text-muted">07:30</span></div>
                <div className="tv-program"><span className="align-top"><a href="#">Пусть говорят</a></span></div>
              </div>
              <div className="p-1 d-flex align-items-start active">
                <div><span className="tv-time align-top mr-2 text-14 text-muted">09:00</span></div>
                <div className="tv-program"><span className="align-top"><a href="#">Каникулы</a></span></div>
              </div>
              <div className="p-1 d-flex align-items-start">
                <div><span className="tv-time align-top mr-2 text-14 text-muted">12:00</span></div>
                <div className="tv-program"><span className="align-top"><a href="#">Х/ф Холоп</a></span></div>
              </div>
              <div className="p-1 d-flex align-items-start">
                <div><span className="tv-time align-top mr-2 text-14 text-muted">13:00</span></div>
                <div className="tv-program"><span className="align-top"><a href="#">Судьба человека с&nbsp;Борисом Корчевниковым</a></span>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </MyLayout>
  );
}

Epgpage.getInitialProps =  async () => {
  try {
    const res = await fetch('http://192.168.0.28/api/v2/ctv-site-epg', {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
        'User-agent': ''
      }
    })

    return await res.json()
  }
  catch (e) {
    return {err: e}
  }
}

export default Epgpage
