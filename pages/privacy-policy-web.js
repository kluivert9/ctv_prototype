import React from 'react'
import MyLayout from '../components/MyLayout'
import MetaHead from '../components/MetaHead'

const Privacypolicy = () => {
    return(
        <MyLayout>
            <MetaHead
                name={'Политика конфиденциальности'}
                keywords={'Ключевые слова страницы политики конфиденциальности'}
                description={'Описание страницы политики конфиденциальности'}
                canonical={'http://localhost:3000/privacy-policy-web'}
            />
            <div className="container-fluid">
                <h1>Здесь будет страница политики конфиденциальности</h1>
            </div>
        </MyLayout>
    )
}

export default Privacypolicy