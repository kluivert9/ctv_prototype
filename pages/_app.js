import React from 'react'
import { withRedux } from '../lib/redux'
import { fetchChannels } from '../redux/actions/channelsAction'
import 'video.js/dist/video-js.css'
import 'plyr/dist/plyr.css'
import '../source/css/bootstrap.min.css'
import '../source/css/styles.css'
import '../source/css/style.css'

const App = ({Component, pageProps}) => {
    return (
        <>
            <Component {...pageProps}/>
        </>
    )
}

App.getInitialProps = async function(ctx) {
  if(!process.browser)  {
      await fetchChannels(ctx.reduxStore)
  }

  return {
    pageProps: {
      ...(ctx.Component.getInitialProps
        ? await ctx.Component.getInitialProps(ctx)
        : {})
    }
  }
}

export default withRedux(App)
