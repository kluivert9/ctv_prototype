import React, { useState, useEffect } from 'react'
import { useRouter } from 'next/router'
import { useSelector } from 'react-redux'
import fetch from 'isomorphic-unfetch'
import Link from "next/link"
import dynamic from 'next/dynamic'
import MetaHead from '../../components/MetaHead'
import Teleprogram from '../../components/Teleprogram'
import Error from '../_error'
import MyLayout from '../../components/MyLayout'

const PlayerNoSSR = dynamic(
  () => import('../../components/VideoApp'),
  { ssr: false }
)

const Channel = data => {
  const router = useRouter()
  const allChannels = useSelector(state => state.channels.channels)
  let item = allChannels.find(item => item.url_protocol === router.query.url_protocol)
  let [isLive, setIsLive] = useState(true)

  if(item === undefined){
    return <Error title='Нет такого канала'/>
  }

  const [channelUrl, setChannelUrl] = useState({url: item.cdn, isArchive: false})
  const [channelDesc, setChannelDesc] = useState(item.current.desc)

  useEffect(()=> {
    window.scrollTo({
      top: 0,
      behavior: 'smooth'
    })
  }, [channelUrl])

  useEffect(() => {
    setChannelUrl({url: item.cdn, isArchive: false})
    setChannelDesc(item.current.desc)
  }, [item])

  const channelsItems = allChannels.map(item => {
    return (
        <li key={item.id}  className={router.query.url_protocol === item.url_protocol ? 'active' : null}>
          <a onClick={()=> router.push('/channel/[url_protocol]', `/channel/${item.url_protocol}`)}><img src={item.image} className="img-fluid" style={{margin: 'auto', cursor: 'pointer'}}/></a>
        </li>
    )
  })
  const setChannelUrlHandler = (cur, desc) => {
    setIsLive(false)
    setChannelUrl({url: cur, isArchive: true})
    setChannelDesc(desc)
  }

  const clickLiveHandler = e => {
    e.preventDefault()
    setIsLive(true)
    setChannelUrl({url: item.cdn, isArchive: false})
    setChannelDesc(item.current.desc)
  }

  const setIsLiveHandler = (bol) => {
    setIsLive(bol)
  }

  return (
    <MyLayout>
      <MetaHead
        name={'Страница' + ' - ' + item.name_ru}
        canonical={`http://localhost:3000/channel/${item.url_protocol}`}
        keywords={'Ключевые слова канала ' + item.name_ru}
        description={'Описание канала ' + item.name_ru}
      />
        <div className="container-fluid">
          <div className="row">
            <div className="col-xs-0 col-sm-0 col-md-2 col-lg-2 col-xl-2">
              <ul className="channels-list list-unstyled border-0 rounded-0 p-0 d-none d-md-block d-lg-block" style={{height: '100vh', overflow: 'auto'}}>
                {channelsItems}
              </ul>
            </div>
            <div className="col-xs-0 col-sm-0 col-md-7 col-lg-7 col-xl-7 px-sm-2 px-md-2 p-lg-0 p-xl-0">
              <div className="mb-4" style={{width: '100%'}}>
                {process.browser
                  ? <PlayerNoSSR
                    channelUrl={channelUrl}
                    useUxternalPlayer={item.use_external_player}
                    externalPlayerCode={item.external_player_code}/>
                  : <div style={{backgroundColor: 'tomato'}}>Load</div>}
              </div>
              <p style={{textAlign: 'center'}}>
              <a
                href='#'
                onClick={e => clickLiveHandler(e)}>прямой эфир</a>
              </p>
              <div className="row">
                <div className="col-8">
                  <nav className="channel-nav">
                    <ul className="list-inline border-0 rounded-0 p-0">
                      <li className="list-inline-item"><a href="#" className="text-xs-center active">О канале</a></li>
                      <li className="list-inline-item"><a href="#" className="text-xs-center">О передаче</a></li>
                    </ul>
                  </nav>
                </div>
                <div className="col-4 text-right"><span><a href="#" className="text-xs-center">В избранное</a></span>
                </div>
              </div>
              <div className="row">
                <div className="col-12">
                  <ul className="list-inline border-0 rounded-0 p-0">
                    <li className="list-inline-item"><a href="#" className="text-white-50 text-xs-center">#тег 1</a>
                    </li>
                    <li className="list-inline-item"><a href="#" className="text-white-50 text-xs-center">#тег 2</a>
                    </li>
                    <li className="list-inline-item"><a href="#" className="text-white-50 text-xs-center">#тег 3</a>
                    </li>
                    <li className="list-inline-item"><a href="#" className="text-white-50 text-xs-center">#тег 4</a>
                    </li>
                    <li className="list-inline-item"><a href="#" className="text-white-50 text-xs-center">#тег 5</a>
                    </li>
                  </ul>
                </div>
              </div>
              <p>{channelDesc}</p>
              <ul className="list-inline border-0 rounded-0 p-0">
                <li className="list-inline-item">
                  <a href="#" className="text-xs-center">
                    <img src={require('../../source/img/i1.png')} className="img-fluid my-1" alt=""/>
                  </a>
                </li>
                <li className="list-inline-item">
                  <a href="#" className="text-xs-center">
                    <img src={require('../../source/img/i2.png')} className="img-fluid my-1" alt=""/>
                  </a>
                </li>
                <li className="list-inline-item">
                  <a href="#" className="text-xs-center">
                    <img src={require('../../source/img/i3.png')} className="img-fluid my-1" alt=""/>
                  </a>
                </li>
              </ul>
            </div>
            <div className="col-xs-0 col-sm-0 col-md-3 col-lg-3 col-xl-3">
              <Teleprogram
                setIsLiveHandler = {bol => setIsLiveHandler(bol)}
                isLive = {isLive}
                urlArchive = {item.url_archive}
                channelProgramObj = {data}
                setChannelUrl = {setChannelUrlHandler}
                router = {router.query.url_protocol}
              />
              <div className="card">
                <div className="card-body">
                  <h4 className="card-title">Скачай приложение</h4>
                  <p className="mt-3 text-white-50">Lorem ipsum dolor sit amet, consetetur sadipscing elitr, diam nonumy
                    eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua.</p>
                  <p className="mt-3">
                    <img src={require('../../source/img/google_play.png')} className="img-fluid my-1" alt=""/>&emsp;
                    <img src={require('../../source/img/app_store.png')} className="img-fluid my-1" alt=""/>
                  </p>
                </div>
              </div>
              <div className="card justify-content-center p-xs-0 p-sm-1 p-md-4 p-xl-5 mt-4">
                <div className="card-body text-center text-white-50"> Здесь могла бы быть Ваша реклама</div>
              </div>
            </div>
          </div>
        </div>
    </MyLayout>
  )
}

Channel.getInitialProps =  async ctx => {
  const urlProt = ctx.ctx.query.url_protocol
  const stateChannels = ctx.reduxStore.getState()
  const currentChannel = stateChannels.channels.channels.find(item => item.url_protocol === urlProt)
  if (currentChannel === undefined) {
    return
  }
  const epgId = currentChannel.id

  try {
    const res = await fetch(`http://pl.iptv2021.com/api/v1/epg?id=${epgId}`, {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
        'User-agent': ''
      }
    })

    return await res.json()
  }
  catch (e) {
    //return {err: e}
    return {}
  }
}

export default Channel
