import React from 'react'
import MyLayout from '../components/MyLayout'
import MetaHead from '../components/MetaHead'

const Aboutservice = () => {
    return(
        <MyLayout>
            <MetaHead
                name={'О сервисе'}
                keywords={'Ключевые слова страницы о сервисе'}
                description={'Описание страницы о сервисе'}
                canonical={'http://localhost:3000/about'}
            />
            <div className="container-fluid">
                <h1>Здесь будет страница о сервисе</h1>
            </div>
        </MyLayout>
    )
}

export default Aboutservice