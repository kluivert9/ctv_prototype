import React from 'react'
import MyLayout from '../components/MyLayout'
import MetaHead from '../components/MetaHead'

const Contacts = () => {
    return(
        <MyLayout>
            <MetaHead
                name={'Связаться с нами'}
                keywords={'Ключевые слова страницы связаться с нами'}
                description={'Описание страницы связаться с нами'}
                canonical={'http://localhost:3000/contacts'}
            />
            <div className="container-fluid">
                <h1>Здесь будет страница связаться с нами</h1>
            </div>
        </MyLayout>
    )
}

export default Contacts