import React from 'react'
import MyLayout from '../components/MyLayout'
import MetaHead from '../components/MetaHead'

const Faq = () => {
    return(
        <MyLayout>
            <MetaHead
                name={'Частые вопросы'}
                keywords={'Ключевые слова страницы частых вопросов'}
                description={'Описание страницы частых вопросов'}
                canonical={'http://localhost:3000/faq'}
            />
            <div className="container-fluid">
                <h1>Здесь будет страница частых вопросов</h1>
            </div>
        </MyLayout>
    )
}

export default Faq