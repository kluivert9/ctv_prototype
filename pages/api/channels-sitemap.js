import { SitemapStream, streamToPromise } from 'sitemap';
import fetch from 'isomorphic-unfetch'
//import { DATA } from '../../source/data';

export default async (req, res) => {
    try {
        const smStream = new SitemapStream({
            hostname: `https://${req.headers.host}`,
            cacheTime: 600000,
        });

        // List of posts
        const responce = await fetch('http://pl.iptv2021.com/api/v2/ctv-site', {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
                'User-agent': ''
            }
        })
        const data = await responce.json()
        const channels = data.channels

        smStream.write({
            url: '/',
            changefreq: 'daily',
            priority: 1
        });
        smStream.write({
            url: '/epgpage',
            changefreq: 'daily',
            priority: 1
        });
        smStream.write({
            url: '/about',
            changefreq: 'daily',
            priority: 1
        });
        smStream.write({
            url: '/privacy',
            changefreq: 'daily',
            priority: 1
        });
        smStream.write({
            url: '/question',
            changefreq: 'daily',
            priority: 1
        });
        smStream.write({
            url: '/contacts',
            changefreq: 'daily',
            priority: 1
        });

        // Create each URL row
        channels.forEach(channel => {
            smStream.write({
                url: `/channel/${channel.url_protocol}`,
                changefreq: 'daily',
                priority: 1
            });
        });

        // End sitemap stream
        smStream.end();

        // XML sitemap string
        const sitemapOutput = (await streamToPromise(smStream)).toString();

        // Change headers
        res.writeHead(200, {
            'Content-Type': 'application/xml'
        });

        // Display output to user
        res.end(sitemapOutput);
    } catch(e) {
        console.log(e)
        res.send(JSON.stringify(e))
    }
}