import React from 'react'
import { useSelector } from 'react-redux'
import MetaHead from '../components/MetaHead'
import MyLayout from '../components/MyLayout'
import ChannelTile from "../components/ChannelTile"

const IndexPage = () => {
    const allChannels = useSelector(state => state.channels.channels)
    const item = allChannels.map(item => (
          <ChannelTile
            key={item.id}
            channel={item.name_ru}
            url={item.url_protocol}
            img={item.image}
            description = {item.current.desc}
            title = {item.current.title}
            timestart = {item.current.timestart}
            timestop = {item.current.timestop}
          />
    ))

    return (

      <MyLayout>
        <MetaHead
          name={'Главная'}
          keywords={'Ключевые слова главной страницы'}
          description={'Описание главной страницы'}
          canonical={'http://localhost:3000'}
        />
          <div className="album">
            <div className="container-fluid">
              <div className="row">
                {item}
              </div>
            </div>
          </div>
      </MyLayout>

    )
}

export default IndexPage
