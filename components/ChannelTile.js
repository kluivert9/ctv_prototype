import React, { useEffect, useState } from 'react'
import Link from 'next/link'
import { useRouter } from 'next/router'

const customStyle = {
  'Матч!': {
    backColor: '#000000',
    textColor: '#ffffff'
  },
  'Спас': {
    backColor: '#0084b0',
    textColor: '#ffffff'
  },
  'СТС': {
    backColor: '#ffffff',
    textColor: '#000000'
  },
  'ТНТ': {
    backColor: '#ffffff',
    textColor: '#000000'
  }
}

const ChannelTile = ({channel, url, img, description, title, timestart, timestop}) => {
  const [isMobile, setIsMobile] = useState(false)
  const begin = new Date(timestart*1000).toLocaleTimeString('ru', {hour: '2-digit', minute:'2-digit'})
  const end = new Date(timestop*1000).toLocaleTimeString('ru', {hour: '2-digit', minute:'2-digit'})
  const router = useRouter()

  useEffect(() => {
   if(/Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent)){
     setIsMobile(true)
   }
  }, [])

  const handleClick = () => {
    if(isMobile){
      router.push('/channel/[url_protocol]', `/channel/${url}`)
    }
  }

  return (
    <div
      className="col-xs-1 col-sm-5 col-md-4 col-lg-3 col-xl-2 mx-sm-auto mx-md-0"
      onClick={handleClick}>
      <div
        className="tv-channels-list card mb-4 border-0 rounded-0 img-fluid"
        style={customStyle[channel] ? {background: customStyle[channel].backColor} : null}
      >
        <img className="card-img rounded-0 tile-img" src={img} alt={channel}/>
        <div
          className="card-img-overlay text-white d-flex flex-column"
          style={customStyle[channel] ? {color: customStyle[channel].textColor} : null}>
          <h4
            className="card-title d-md-none d-lg-block"
            style={customStyle[channel] ? {color: customStyle[channel].textColor} : null}>
              {channel}
          </h4>
          <div className="card-img-overlay d-flex flex-column justify-content-end" style={customStyle[channel] ? {color: customStyle[channel].textColor} : null}>
            <div className="card-sub">{title}</div>
            <div className="card-time">
              <span style={customStyle[channel] ? {color: customStyle[channel].textColor} : null}>
                {begin}-{end}
              </span>
            </div>
          </div>
        </div>
        { !isMobile &&
        <div className="inner-overlay text-left text-white-50" style={{ overflow: 'auto' }}>
          <div className="card-fav float-right"><a href="#"><small><i className="fas fa-star"></i></small></a>
          </div>
          <h4 className="card-title font-weight-bold">{channel}</h4>
          <div className="card-sub pt-1">{title}</div>
          <Link href="/channel/[url_protocol]" as={`/channel/${url}`}>
            <p className="pt-1 my-0 tile-text">
              {description}
            </p>
          </Link>
          <div className="card-footer p-0 border-0">
            <Link href="/channel/[url_protocol]" as={`/channel/${url}`}>
              <a className="text-underline tile-text">Подробнее</a>
            </Link>
          </div>
        </div>
        }
      </div>
    </div>
  )
}

export default ChannelTile
