import React from 'react'
import Head from 'next/head'

const MetaHead = (props) => (
    <Head>
      <title>{props.name}</title>
      <link rel="shortcut icon" href="/nextjs.ico" />
      <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
      <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
      <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.0/css/all.css" integrity="sha384-lZN37f5QGtY3VHgisS14W3ExzMWZxybE1SJSEsQp9S+oqd12jhcu+A56Ebc1zFSJ" crossOrigin="anonymous" />
      <script src="https://imasdk.googleapis.com/js/sdkloader/ima3.js"></script>
      <script src="/js/jquery.min.js"></script>
      <script src="/js/bootstrap.min.js"></script>

        {props.keywords
        ? <meta name="keywords" content={props.keywords} />
        : null
        }

        {props.description
        ? <meta name="description" content={props.description} />
        : null
        }

        {props.canonical
        ? <link rel="canonical" href={props.canonical}/>
        : null
        }
    </Head>
)

export default MetaHead
