import React, { useState, useRef, useEffect } from 'react'

const Teleprogram = ({channelProgramObj, setChannelUrl, urlArchive, isLive, setIsLiveHandler, router}) => {
  const [channelProgram, setChannelProgram] = useState(Object.values(channelProgramObj))

  if (!channelProgram.length) {
    return (
      <div className="tv-program-list">
        <div className="card border-0 rounded-0 mb-4 p-2">
          <div className='p-1 d-flex align-items-start'>
            <span>Нет телепрограммы</span>
          </div>
        </div>
      </div>
    )
  }

  const [currentDay, setCurrentDay] = useState(channelProgram.findIndex(item => item.active))
  let lastDayKey = useRef(currentDay)
  let lastProgramKey = useRef(null)
  let liveDayKey = useRef(currentDay)
  let liveProgramKey = useRef(channelProgram[currentDay].data.findIndex(item => item.current))

  useEffect( () => {
    setChannelProgram(Object.values(channelProgramObj))
    setCurrentDay(Object.values(channelProgramObj).findIndex(item => item.active))
    lastDayKey.current = Object.values(channelProgramObj).findIndex(item => item.active)
    liveDayKey.current = lastDayKey.current
    liveProgramKey.current = Object.values(channelProgramObj)[liveDayKey.current].data.findIndex(item => item.current)
  }, [router])

  useEffect(() => {
    if (isLive) {
      const newChannelProgram = channelProgram.slice(0)
      newChannelProgram[lastDayKey.current].data[lastProgramKey.current].current = false
      newChannelProgram[liveDayKey.current].data[liveProgramKey.current].current = true
      lastDayKey.current = liveDayKey.current
      lastProgramKey.current = liveProgramKey.current
      setChannelProgram(newChannelProgram)
      setCurrentDay(liveDayKey.current)
    }
  }, [isLive])

  const clickEpgPrev = e => {
    e.preventDefault()
    if (currentDay - 1 >= 0) {
      setCurrentDay(currentDay - 1)
      setIsLiveHandler(false)
    }
  }

  const clickEpgNext = e => {
    e.preventDefault()
    if (currentDay + 1 < channelProgram.length) {
      setCurrentDay(currentDay + 1)
      setIsLiveHandler(false)
    }
  }

  const arrCurrentDayProgram = channelProgram[currentDay].data.map((item, index) => {
    const urlArchiveLink = `${urlArchive}index-${item.begin}-${item.end - item.begin}.m3u8`
    const hasArchive = (item.begin <= Math.round(Date.now() / 1000) || item.current) && urlArchive
    let itemClasses = 'p-1 d-flex align-items-start '

    !hasArchive ?  itemClasses += 'mute ' : null

    if (item.current) {
      lastProgramKey.current = index
      itemClasses += 'active'
    }

    return (
      <div
        className={itemClasses}
        key={index}
        onClick={()=> {hasArchive ? setProgramHandler(index) : null; hasArchive ? setChannelUrl(urlArchiveLink, item.desc) : null}}>
        <div><span className="tv-time align-top mr-2 text-14 text-white-50">{item.time}</span></div>
        <div className="tv-program">
          <span className="align-top">
            <a href="#" onClick={e => e.preventDefault()}>{item.title}</a>
          </span>
        </div>
      </div>
    )
  })

  const setProgramHandler = index => {
    const newChannelProgram = channelProgram.slice(0)
    newChannelProgram[lastDayKey.current].data[lastProgramKey.current].current = false
    newChannelProgram[currentDay].data[index].current = true
    lastDayKey.current = currentDay
    lastProgramKey.current = index
    setChannelProgram(newChannelProgram)
  }

  return (
    <div className="tv-program-list" style={{borderRadius: '.25rem'}}>
      <div className="row p-2">
        <div className="col-2 text-right">
          <a href="#" onClick={e => clickEpgPrev(e)}><i className="fas fa-angle-left"></i></a>
        </div>
        <div className="col-8 text-center text-truncate">{channelProgram[currentDay].name}</div>
        <div className="col-2 text-left">
          <a href="#" onClick={e => clickEpgNext(e)}><i className="fas fa-angle-right"></i></a>
        </div>
      </div>
      <div className="card border-0 rounded-0 mb-4 p-2" style={{maxHeight: '500px', overflow: 'auto' }}>
        {arrCurrentDayProgram}
      </div>
    </div>
  )
}
export default Teleprogram
