import React from 'react'
import Header from './Header'
import Link from 'next/link';

const MainLayout = props => (
    <>
      <Header />
      <main role="main">
        {props.children}
      </main>
      <footer className="clearfix mt-2">
        <div className="container-fluid">
          <nav className="navbar navbar-expand-sm">
            <ul className="navbar-nav mr-auto">
              <li className="nav-item mr-2"><Link href="/about"><a className="nav-link">О сервисе</a></Link></li>
              <li className="nav-item mr-2"><Link href="/privacy-policy-web"><a className="nav-link">Политика конфиденциальности</a></Link></li>
              <li className="nav-item mr-2"><Link href="/faq"><a className="nav-link">Частые вопросы</a></Link></li>
              <li className="nav-item mr-2"><Link href="/contacts"><a className="nav-link">Связаться с нами</a></Link></li>
            </ul>
            <span className="float-right"><a href="https://limehd.tv" className="text-white-50">ООО Лайм Эйч Ди, 2020. v0.0.2</a></span>
          </nav>
        </div>
      </footer>
    </>
  );

export default MainLayout
