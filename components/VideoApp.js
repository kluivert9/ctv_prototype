import React, { useEffect, useState, useRef } from 'react'
import 'videojs-contrib-ads'
import 'videojs-ima'
import videojs from "video.js"
import qualityLevels from 'videojs-contrib-quality-levels'
import hlsQualitySelector from 'videojs-hls-quality-selector'

const VideoApp = ({channelUrl, useUxternalPlayer, externalPlayerCode}) => {
    let showExpPlayer = false/*useUxternalPlayer*/
    let videoWrapperNode = null
    let videoNode = null
    const [player, setPlayer] = useState(null)
    const [adsManager, setAdsManager] = useState(null)
    let currAdsKey = useRef(0)
    const arrAds = [
      //'https://data.videonow.ru/?profile_id=2458118&format=vast&container=preroll',
      //'https://an.yandex.ru/meta/347075?imp-id=2&charset=UTF-8&target-ref=http%3A%2F%2Flimehd.ru&page-ref=http%3A%2F%2Flimehd.ru&rnd=j89gjdg985gjhrtghk956p',
      //'https://data.videonow.ru/?profile_id=2458118&format=vast&container=preroll',
      //'http://pubads.g.doubleclick.net/gampad/ads?sz=640x480&iu=/124319096/external/ad_rule_samples&ciu_szs=300x250&ad_rule=1&impl=s&gdfp_req=1&env=vp&output=xml_vmap1&unviewed_position_start=1&cust_params=sample_ar%3Dpremidpostpod%26deployment%3Dgmf-js&cmsid=496&vid=short_onecue&correlator='
    ]

    /*useEffect(() => {
      if (showExpPlayer) {
        videoWrapperNode.innerHTML = '<div class="frame-wr">' + externalPlayerCode+ '</div>'
      }
    }, [channelUrl.url])*/

  useEffect(() => {
    if (!showExpPlayer){
      const options = {
        muted: false,
        sources : [{

          src: channelUrl.url,
          type: 'application/x-mpegURL'
        }],
        controls: true,
        controlBar: {
          pictureInPictureToggle: false
        },
        autoplay: true
      }
      let pl = videojs(videoNode, options, () => {
        pl.volume(1)
        pl.hlsQualitySelector({displayCurrentQuality: false})
      })
      setPlayer(pl)
      let adsMan = null

      if (typeof(google) !== 'undefined' && arrAds.length) {
        const imaOptions = {
          id: 'content_video',
          adLabel: '',
          adTagUrl: arrAds[currAdsKey.current]
        }

        pl.ima(imaOptions)

        pl.on("adsready", () => {
          adsMan = pl.ima.getAdsManager()
          setAdsManager(adsMan)

          pl.ima.addEventListener(google.ima.AdEvent.Type.STARTED, () => {
            //console.log('start')
          })

          pl.ima.addEventListener(google.ima.AdEvent.Type.COMPLETE, () => {
            //console.log('complete')
            pl.play()
          })

          pl.ima.addEventListener(google.ima.AdEvent.Type.SKIPPED, () => {
            //console.log('skip')
            pl.play()
          })

          pl.ima.addEventListener(google.ima.AdEvent.Type.CLICK, () => {
            //console.log('click')
            adsMan.stop()
            pl.play()
          })

          pl.ima.addEventListener(google.ima.AdEvent.Type.LOADED, () => {
            //console.log('load')
          })

        })

        pl.on('adserror', () => {
          if (currAdsKey.current >= arrAds.length) {
            currAdsKey.current = 0
            pl.play()

            return
          }

          pl.ima.changeAdTag(arrAds[++currAdsKey.current])
          pl.ima.requestAds()
        })
      }

      return () => {
        if(pl) {
          pl.dispose()
        }

        if(adsMan) {
          adsMan.destroy()
        }
      }
    }
  }, [])

  useEffect(() => {
    currAdsKey.current = 0
    if (player) {
      if(adsManager) {
        adsManager.stop()
      }

      player.src(channelUrl.url)
      if (typeof(google) !== 'undefined' && arrAds.length) {
        player.ima.changeAdTag(arrAds[currAdsKey.current])
        player.ima.requestAds()
      }
    }
  }, [channelUrl.url])

  return (
          <div className='video-wr' ref={node => videoWrapperNode = node} >
              {!showExpPlayer && <video
                ref={node => videoNode = node}
                id="content_video"
                className="video-js vjs-custom-skin"
              />}
          </div>
        );

}
export default VideoApp;
