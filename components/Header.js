import React from 'react'
import Link from 'next/link'
import logo from '../source/img/logo.png'
import { useRouter } from 'next/router'

const HeaderMenu = () => {
  const router = useRouter()

    return(
      <header className="clearfix mb-3 p-3">
        <nav className="navbar navbar-main navbar-expand-md navbar-dark">
          <div className="container-fluid">
            <Link href="/">
              <a href="index.html" className="navbar-brand"><img src={logo} alt="logo"/></a>
            </Link>
            <button className="navbar-toggler" type="button" data-toggle="collapse"
                    data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false"
                    aria-label="Toggle navigation"><span className="navbar-toggler-icon"></span></button>
            <div className="collapse navbar-collapse" id="navbarSupportedContent">
              <ul className="navbar-nav mx-auto">
                <li className="nav-item">
                  <Link href="/">
                    <a className={router.pathname === '/' ? 'nav-link active' : 'nav-link'}>Цифровое ТВ</a>
                  </Link>
                </li>
                <li className="nav-item">
                  <Link href="/epgpage">
                    <a className={router.pathname === '/epgpage' ? 'nav-link active' : 'nav-link'}>Телепрограмма</a>
                  </Link>
                </li>
              </ul>
            </div>
          </div>
        </nav>
      </header>
    )
}

export default HeaderMenu
