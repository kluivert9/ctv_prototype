import React, { useEffect, useState, useRef } from 'react'
import Hls from 'hls.js'
import Plyr from 'plyr';

const VideoApp = ({channelUrl, useUxternalPlayer, externalPlayerCode}) => {
    let showExpPlayer = false/*item.use_external_player*/
    let videoWrapperNode = useRef()
    let videoNode = null

    /*useEffect(() => {
      if (showExpPlayer) {
        videoWrapperNode.innerHTML = item.external_player_code
      }
    })*/
    useEffect(() => {
      if (!showExpPlayer){
        videoNode = document.createElement('video')
        videoWrapperNode.current.appendChild(videoNode)
        const source = channelUrl.url
        let controls = null

        if(channelUrl.isArchive){
          controls = ['play-large', 'play', 'current-time', 'progress', 'duration',  'mute', 'volume', 'captions', 'settings', 'fullscreen']
        }else{
          controls = ['play-large', 'play',  'mute', 'volume', 'captions', 'settings', 'fullscreen']
        }

        const options = {
        ads: {
          enabled: false, tagUrl: 'https://an.yandex.ru/meta/347075?imp-id=2&charset=UTF-8&target-ref=http%3A%2F%2Flimehd.ru&page-ref=http%3A%2F%2Flimehd.ru&rnd=j89gjdg985gjhrtghk956p'},
        controls: controls,
        autoplay: true,
        autopause: false,
        volume: 0,
        muted: true,
        ratio: '16:9',
        hideControls: true,
        settings: ["quality", "speed"],
        quality: {default : 576, forced: true, options: [576, 480, 360], selected: 576 },
        speed: { selected: 1, options: [ ] }
      }

        const pl = new Plyr(videoNode, options)
        const hls = new Hls()

        if (!Hls.isSupported()) {
          videoNode.src = source
        } else {         
          hls.loadSource(source)
          hls.attachMedia(videoNode)
          console.log(Hls.Events)
          hls.on('hlsLevelLoaded', function () {
            console.log("video and hls.js are now bound together !");
          })
          //  hls.currentLevel = 2 макс качество сразу
          //  hls.currentLevel = 0 мин качество сразу
          hls.currentLevel = 2
          
          pl.on('qualitychange', () => {
            console.log('qualitychange')
            hls.currentLevel = pl.quality
          });

          pl.on('languagechange', () => {
            setTimeout(() => hls.subtitleTrack = pl.currentTrack, 50);
          });

          pl.on("adsloaded", () => {
            console.log('adsloaded')
          })
          pl.on("adscontentpause", () => {
            console.log('adscontentpause')
          })
          pl.on("adstarted", () => {
            console.log('adstarted')
          })
          pl.on("adsmidpoint", () => {
            console.log('adsmidpoint')
          })
          pl.on("adscomplete", () => {
            console.log('adscomplete')
          })
          pl.on("adscontentresume", () => {
            console.log('adscontentresume')
          })
          pl.on("adsclick", () => {
            console.log('adsclick')
          })
          pl.on("adsimpression", () => {
            console.log('adsimpression')
          })

          pl.on('ready', () => {
            console.log('ready')
          });
          pl.on('play', () => {
            console.log('play')
          });
          pl.on('pause', () => {
            console.log('pause')
          });
          pl.on('playing', () => {
            console.log('playing')
          });
        }

        return () => {
          if (pl) {
            console.log('asdas')
            pl.destroy()
            hls.destroy()
            videoWrapperNode.current.innerHTML = ''
          }
        }
      }
    }, [channelUrl])

        return (
          <div style={{width: '720px', height: '420px'}} ref={node => videoWrapperNode.current = node} ></div>
        );

}
export default VideoApp;
