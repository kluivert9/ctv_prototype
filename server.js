const cacheableResponse = require('cacheable-response')
const express = require('express')
const next = require('next')

const port = parseInt(process.env.PORT, 10) || 3000
const dev = process.env.NODE_ENV !== 'production'
const app = next({ dev })

const handle = app.getRequestHandler()

const ssrCache = cacheableResponse({
  ttl: 1000 * 60 , // 1min
  get: async ({ req, res, pagePath, queryParams }) => {
    const data = await app.renderToHTML(req, res, pagePath, queryParams)
    const ttl = 1000 * 60 // 1min
    const headers = { cacheControl: 'public, no-cache, max-age=60' }

    if (res.statusCode === 404) {
      res.end(data)
      return
    }

    return { data, ttl, headers }
  },
  send: ({ data, res, headers }) => {
    res.setHeader('Cache-Control', headers.cacheControl)
    res.send(data)
  },
})

app.prepare().then(() => {
  const server = express()

  server.get('/', (req, res) => ssrCache({ req, res, pagePath: '/' }))
  server.get('*', (req, res) => handle(req, res))

  server.listen(port, err => {
    if (err) throw err
    console.log(`> Ready onnn http://localhost:${port}`)
  })
})
