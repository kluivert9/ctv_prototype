import { GET_CHANNELS } from "./actionTypes"
import fetch from 'isomorphic-unfetch'
import { DATA } from "../../source/data"

export async function fetchChannels(store) {
     const res = await fetch('http://pl.iptv2021.com/api/v2/ctv-site', {
         method: 'POST',
         headers: {
             'Content-Type': 'application/json',
             'User-agent': ''
         }
     })
    const data = await res.json()
    const { dispatch } = store
    dispatch({ type: GET_CHANNELS, payload: data.channels});
}

