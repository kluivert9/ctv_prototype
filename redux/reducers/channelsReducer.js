import { GET_CHANNELS } from "../actions/actionTypes"

const initialState = {
    channels: []
}

export default function channelsReducer(state = initialState, action) {
    switch (action.type) {
        case GET_CHANNELS:
            return {
                channels: action.payload
            }
        default:
            return state
    }
}