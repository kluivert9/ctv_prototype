import { createStore, applyMiddleware } from 'redux'
import { composeWithDevTools } from 'redux-devtools-extension'
import  combineReducers from './reducers/rootReducer'

const initialState = {
    channels: []
}

export const initializeStore = (preloadedState = initialState) => {
    return createStore(
        combineReducers,
        preloadedState,
        composeWithDevTools(applyMiddleware())
    )
}